from django.contrib.auth.models import User
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    notes = serializers.HyperlinkedRelatedField(
        many=True, view_name='note-detail', read_only=True)
    subscriptions = serializers.HyperlinkedRelatedField(
        many=True, view_name='subscription-detail', read_only=True)
    channels = serializers.HyperlinkedRelatedField(
        many=True, view_name='channel-detail', read_only=True)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'username', 'notes',
                  'subscriptions', 'channels', 'date_joined', 'last_login')
        extra_kwargs = {
            'date_joined': {'read_only': True},
            'last_login': {'read_only': True}
        }
