from django.urls import path, include
from rest_framework.routers import DefaultRouter

from notes.views import NoteViewSet, retrieve_public_note


router = DefaultRouter()
router.register(r'notes', NoteViewSet, base_name='note')

urlpatterns = [
    path('', include(router.urls)),
    path('public/<slug:link>/', retrieve_public_note),
]
