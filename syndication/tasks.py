from __future__ import absolute_import, unicode_literals

from datetime import datetime

from django.conf import settings
from django.contrib.auth.models import User
from django.utils import timezone
import feedparser

from feednote_it import celery_app as app
from syndication.models import Article, Channel, Subscription


feedparser.USER_AGENT = settings.USER_AGENT


@app.task
def refresh_channels():
    """
    A periodic task that updates each channel and it saves their latest articles.
    """
    channels = Channel.objects.\
        exclude(is_active=False).values_list('pk', flat=True)

    for channel in channels:
        obtain_articles.delay(channel)


@app.task
def obtain_articles(channel_pk):
    """
    Obtains articles from a channel.
    """
    channel: Channel = Channel.objects.get(pk=channel_pk)
    parsed = feedparser.parse(channel.link, etag=channel.etag)
    if parsed.status != 304:
        http = {
            'etag': parsed.etag if 'etag' in parsed else None,
            'href': parsed.href,
            'status': parsed.status,
        }
        update_channel_info.delay(channel_pk, parsed.feed, **http)

        if parsed.entries:
            for entry in parsed.entries:
                try:
                    article: Article = Article.objects.get(guid=entry.id)
                except Article.DoesNotExist:
                    article: Article = Article()

                guid = entry.get('id')
                if guid != article.guid:
                    article.guid = guid

                published = timezone.make_aware(datetime(
                    *entry.published_parsed[:6])) if 'published_parsed' in entry else None
                if published and published != article.published:
                    article.published = published

                title = entry.get('title')
                if title != article.title:
                    article.title = title

                contents = entry.get('content')
                content = contents[0].get('value') if contents else None
                if content and content != article.content:
                    article.content = content

                summary = entry.get('summary')
                if summary != article.summary:
                    article.summary = summary

                author = entry.get('author')
                if author != article.author:
                    article.author = author

                link = entry.get('link')
                if link != article.link:
                    article.link = link

                _license = entry.get('license')
                if _license != article.license:
                    article.license = _license

                article.channel = channel
                article.updated = timezone.now()
                article.save()


@app.task
def subscribe_to_channel(channel_pk: int, user_pk: int):
    """
    Subscribe an user to a channel.
    """
    channel = Channel.objects.get(pk=channel_pk)
    user = User.objects.get(pk=user_pk)
    Subscription(channel=channel, subscriber=user).save()


@app.task
def update_channel_info(primary_key: int, feed: dict, **kwargs: dict):
    """
    Update channel information.
    """
    etag = kwargs.get('etag')
    status = kwargs.get('status')
    href = kwargs.get('href')

    channel: Channel = Channel.objects.get(pk=primary_key)

    title = feed.get('title')
    if title != channel.title:
        channel.title = title

    published = timezone.make_aware(datetime(
        *feed['published_parsed'][:6])) if 'published_parsed' in feed else None
    if published and published != channel.published:
        channel.published = published

    description = feed.get('subtitle')
    if description != channel.description:
        channel.description = description

    author = feed.get('author')
    if author != channel.author:
        channel.author = author

    publisher = feed.get('publisher')
    if publisher != channel.publisher:
        channel.publisher = publisher

    generator = feed.get('generator')
    if generator != channel.generator:
        channel.generator = generator

    language = feed.get('language')
    if language != channel.language:
        channel.language = language

    _license = feed.get('license')
    if _license != channel.license:
        channel.license = _license

    rights = feed.get('rights')
    if rights != channel.rights:
        channel.rights = rights

    if status != channel.status_code:
        channel.status_code = status

    if etag != channel.etag:
        channel.etag = etag

    if status == 301:
        channel.link = href
    elif status in (404, 410):
        channel.is_active = False

    channel.updated = timezone.now()
    channel.save()
