from django.db import models


# Create your models here.
class Channel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(null=True)
    published = models.DateTimeField(null=True)
    title = models.CharField(max_length=256, null=True)
    description = models.CharField(max_length=500, null=True)
    author = models.CharField(max_length=100, null=True)
    publisher = models.CharField(max_length=50, null=True)
    generator = models.CharField(max_length=50, null=True)
    link = models.URLField(unique=True)
    language = models.CharField(max_length=10, null=True)
    license = models.URLField(null=True)
    rights = models.CharField(max_length=100, null=True)
    is_active = models.BooleanField(default=True)
    status_code = models.IntegerField(null=True)
    etag = models.CharField(max_length=100, null=True)
    created_by = models.ForeignKey(
        'auth.User', related_name='channels', on_delete=models.SET_NULL, null=True)
    subscribers = models.ManyToManyField('auth.User', through='Subscription')

    def __str__(self):
        return self.title

    def get_total_subscribers(self):
        """
        Return the number subscribers total.
        """
        return Subscription.objects.filter(channel=self).count()

    class Meta:
        ordering = ('created',)


class Article(models.Model):
    guid = models.URLField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField()
    published = models.DateTimeField(null=True)
    title = models.CharField(max_length=256)
    content = models.TextField(null=True)
    summary = models.TextField(null=True)
    author = models.CharField(max_length=100, null=True)
    link = models.URLField()
    license = models.URLField(null=True)
    channel = models.ForeignKey(
        Channel, related_name='articles', on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('created',)


class Subscription(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    subscriber = models.ForeignKey(
        'auth.User', related_name='subscriptions', on_delete=models.CASCADE, null=True)
    channel = models.ForeignKey(Channel, on_delete=models.CASCADE)

    class Meta:
        ordering = ('created',)
