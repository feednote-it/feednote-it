module.exports = {
  baseUrl: "/static/client/",
  outputDir: `${__dirname}/client/static/client/`,
  indexPath: `${__dirname}/client/templates/client/index.html`,
  devServer: {
    proxy: {
      "/api*": {
        target: "http://127.0.0.1:8000/"
      }
    }
  }
};
